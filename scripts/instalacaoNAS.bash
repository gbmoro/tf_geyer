cd $HOME/benchs/
wget https://www.nas.nasa.gov/assets/npb/NPB3.0.tar.gz 
tar vxf NPB3.0.tar.gz
cd NPB3.0/NPB3.0-OMP/config/
cp make.def.template make.def
cp suite.def.template suite.def
sed -i 's/=\ f77/=\ gfortran\ -fopenmp/g' make.def
sed -i 's/S/B/g' suite.def
cd ..
make suite

for i in $(ls bin); do
	mv bin/$i bin/$i-gfortran
done

cd config
sed -i 's/gfortran/scorep-gfortran/g' make.def
cd ..
make suite

for i in $(ls bin); do
if echo "$i" | egrep "gfortran" >/dev/null
then
    continue;
else
	  mv bin/$i bin/$i-scorep
fi
done
