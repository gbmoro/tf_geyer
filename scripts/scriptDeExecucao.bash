#!/bin/bash

pathOfNAS="/home/gabrielbmoro/benchs/NPB3.0/NPB3.0-OMP/bin"

apps=("ft" "cg")

export OMP_NUM_THREADS=40
export GOMP_CPU_AFFINITY=0-40

for i in ${apps[@]}; do

	export SCOREP_ENABLE_PROFILING=true 
	export SCOREP_ENABLE_TRACING=true 
	export SCOREP_TOTAL_MEMORY=3G 
	export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_DCM,PAPI_L2_ICM 
	export SCOREP_METRIC_RUSAGE=ru_utime,ru_stime
	export SCOREP_EXPERIMENT_DIRECTORY=../data/$i-40threads_exp4
	
	sudo -E LD_LIBRARY_PATH=/home/gabrielbmoro/Programs/scorep-3.0/build/lib: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7: \
/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin: \
/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 \
$pathOfNAS/$i.B-scorep

done
