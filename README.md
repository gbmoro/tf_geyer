# Estrutura da Pasta de Projeto
-----
**Autor:** Gabriel Bronzatti Moro
**Professor:** Claudio Fernando Resin Geyer
**Disciplina:** Programação Paralela e Distribuída
---
Essa pasta está organizada da seguinte forma:

  - img: contém os gráficos dos experimentos
  - scripts: contém os scripts utilizados nos experimentos e também na configuração de ambiente
  - LabBook.org: arquivo que define passo-a-passo todas as atividades realizadas, a partir desse arquivo é possível reproduzir toda a metodologia do trabalho

Mais informações sobre org em: https://mescal.imag.fr/membres/arnaud.legrand/misc/init.php
