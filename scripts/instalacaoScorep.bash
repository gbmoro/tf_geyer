cd $HOME/Downloads/
wget http://www.vi-hps.org/upload/packages/scorep/scorep-3.0.tar.gz
tar vxf scorep-3.0.tar.gz
cd scorep-3.0/
mkdir build/
./configure --prefix=$HOME/Programs/scorep-3.0/build/ --enable-papi --with-papi-lib=/usr/local/lib/ --with-papi-header=/usr/local/include/ --without-gui --without-shmen
make -j4 > /dev/null  2> merr
cat merr
sudo make install > /dev/null 2> mierr 
cat mierr 
sed -i '1iexport PATH=$HOME/Programs/scorep-3.0/build/bin:$PATH' ~/.bashrc
sed -i '1iexport LD_LIBRARY_PATH=$HOME/Programs/scorep-3.0/build/lib:$LD_LIBRARY_PATH'  ~/.bashrc
